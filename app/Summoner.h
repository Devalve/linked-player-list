#pragma once

#include "config.h"
#include "Mage.h"


class Summoner : public Mage {
protected:
    char creature[MAX_STRING_LENGTH];
public:
    Summoner(const char*, uint16_t, uint32_t, uint32_t, double, uint32_t, uint32_t, const char*);
    Summoner();

    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);
    
    const char* get_creature() const;
};
