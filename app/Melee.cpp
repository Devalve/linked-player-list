#include <iostream>
#include <cstring>

#include "Melee.h"


Melee::Melee(
    const char* name,
    uint16_t level,
    uint32_t hp,
    uint32_t max_hp,
    double gold,
    uint32_t strength,
    uint16_t attack_speed
) : Warrior(name, level, hp, max_hp, gold, strength), attack_speed(attack_speed) {
    classname = MELEE;
}


Melee::Melee() : attack_speed(150) {
    classname = MELEE;
}


void Melee::print(std::ostream& ostream) const {
    Warrior::print(ostream);
    ostream << ", attack_speed=" << attack_speed << std::endl;
}


void Melee::save(std::ofstream& fout) {
    Warrior::save(fout);
    fout.write(reinterpret_cast<char*>(&attack_speed), sizeof(attack_speed));
}


void Melee::load(std::ifstream& fin) {
    Warrior::load(fin);
    fin.read(reinterpret_cast<char*>(&attack_speed), sizeof(attack_speed));
}


uint16_t Melee::get_attack_speed() const {
    return attack_speed;
}
