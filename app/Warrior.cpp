#include <iostream>
#include <cstring>

#include "Warrior.h"


Warrior::Warrior(
    const char* name,
    uint16_t level,
    uint32_t hp,
    uint32_t max_hp,
    double gold,
    uint32_t strength
) : BasePlayer(name, level, hp, max_hp, gold), strength(strength) {
    classname = WARRIOR;
}


Warrior::Warrior() : strength(10) {
    classname = WARRIOR;
}


void Warrior::print(std::ostream& ostream) const {
    BasePlayer::print(ostream);
    ostream << ", strength=" << strength;
}


void Warrior::save(std::ofstream& fout) {
    BasePlayer::save(fout);
    fout.write(reinterpret_cast<char*>(&strength), sizeof(strength));
}


void Warrior::load(std::ifstream& fin) {
    BasePlayer::load(fin);
    fin.read(reinterpret_cast<char*>(&strength), sizeof(strength));
}


uint32_t Warrior::get_strength() const {
    return strength;
}
