#include <iostream>
#include <cstring>

#include "Attacker.h"


Attacker::Attacker(
    const char* name,
    uint16_t level,
    uint32_t hp,
    uint32_t max_hp,
    double gold,
    uint32_t mana,
    uint32_t max_mana,
    const char* spell
) : Mage(name, level, hp, max_hp, gold, mana, max_mana) {
    strcpy(this->spell, spell);
    classname = ATTACKER;
}


Attacker::Attacker() {
    strcpy(spell, "Fireball");
    classname = ATTACKER;
}


void Attacker::print(std::ostream& ostream) const {
    Mage::print(ostream);
    ostream << ", spell=" << spell << std::endl;
}

void Attacker::save(std::ofstream& fout) {
    Mage::save(fout);
    fout.write(reinterpret_cast<char*>(spell), sizeof(spell));
}


void Attacker::load(std::ifstream& fin) {
    Mage::load(fin);
    fin.read(reinterpret_cast<char*>(spell), sizeof(spell));
}


const char* Attacker::get_spell() const {
    return spell;
}
