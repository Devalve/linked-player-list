#pragma once

#include "Warrior.h"
#include "config.h"


class Melee : public Warrior {
protected:
    uint16_t attack_speed;
public:
    Melee(const char*, uint16_t, uint32_t, uint32_t, double, uint32_t, uint16_t);
    Melee();

    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);

    uint16_t get_attack_speed() const;
};
